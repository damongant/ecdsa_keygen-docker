#!/bin/bash
cd /
virtualenv -p /usr/bin/python2.7 ecdsa_keygen > /dev/null 2> /dev/null
cd ecdsa_keygen
source bin/activate > /dev/null 2> /dev/null
git clone https://github.com/seandsanders/ecdsa_keygen > /dev/null 2> /dev/null
(cd ecdsa_keygen; python setup.py develop) > /dev/null 2> /dev/null
python ecdsa_keygen/ecdsa_keygen.py